/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.*;
/**
 * Represent a list of counters with a queue per counter objectYou can queue and dequeue to a counter identified by a counter object.
 * NOTE: This class is not thread-safe
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: CounterQueue.java,v 1.3 2005/04/26 17:41:24 thlee Exp $ $Name:  $
 */
public class CounterQueue
{

    /** the counters * */
    private HashMap _counters = new HashMap();

    /**
     * {@inheritDoc}
     */
    public CounterQueue()
    {
    }

    /**
     * Get the list for the counter object
     * @param o the counter object
     * @return the list, or null if there's none
     */
    private List _getList(Object o)
    {
        if (_counters.containsKey(o))
        {
            return (List) _counters.get(o);
        } else
        {
            return null;
        }
    }

    /**
     * Get the (unmodifiable) queue of the counter object
     * @param o the counter object
     * @return the queue
     */
    public List getQueue(Object o)
    {
        if (_counters.containsKey(o))
        {
            return Collections.unmodifiableList((List) _counters.get(o));
        } else
        {
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * Clear the queue of the counter object
     * @param o the counter object
     * @return the queue for the counter object
     */
    public List clearQueue(Object o)
    {
        Object queue = _counters.remove(o);
        if (queue != null)
        {
            return (List) queue;
        } else
        {
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * Add an queuer to the queue of the counter object. A queue will be created if there's none for the counter object
     * @param o the counter object
     * @param queuer the queue
     * @return true
     */
    public boolean addToQueue(Object o, Object queuer)
    {
        List l = _getList(o);
        if (l == null)
        {
            l = new ArrayList();
            _counters.put(o, l);
        }
        return l.add(queuer);
    }

    /**
     * Remove the next queuer in the queue. Null if queue is empty
     * @param counter the counter
     * @return the next queuer in the counter, or null if queue is empty
     */
    public Object removeNextFromQueue(Object counter)
    {
        List l = _getList(counter);
        if (l == null || l.size() == 0)
        {
            return null;
        } else
        {
            return l.remove(0);
        }
    }

    /**
     * Remove the queuer from the queue of the counter object. If there's no queue for the counter object, it will do nothing and
     * return false.
     * @param o the counter object
     * @param queuer the queuer
     * @return true if list contains the element
     */
    public boolean removeFromQueue(Object o, Object queuer)
    {
        List l = _getList(o);
        if (l != null)
        {
            return l.remove(queuer);
        } else
        {
            return false;
        }
    }
}