/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

/**
 * Represent a map key that consists of two value
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class DuoMapKey
{

    private final int _hashCode;

    /** key1 **/
    private Object key1;

    /** key2 **/
    private Object key2;

    /**
     * Constructor
     * @param key1
     * @param key2
     */
    public DuoMapKey(Object key1, Object key2)
    {
        _hashCode = ((key1 == null) ? 0 : key1.hashCode()) + ((key2 == null) ? 0 : key2.hashCode() >> 4);
    }

    /** 
     * {@inheritDoc}
     */
    public int hashCode()
    {
        return _hashCode;
    }

    /** 
     * {@inheritDoc}
     */
    public boolean equals(Object o)
    {
        if (o instanceof DuoMapKey)
        {
            DuoMapKey other = (DuoMapKey) o;
            return _keyEquals(key1, other.key1) && _keyEquals(key2, other.key2);
        } else
        {
            return false;
        }
    }

    /**
     * Return whether key and other is equals
     * @param key the key
     * @param other the other key
     * @return true if both null or equals
     */
    private boolean _keyEquals(Object key, Object other)
    {
        if (key == null)
        {
            return other == null;
        } else
        {
            return key.equals(other);
        }
    }
}
