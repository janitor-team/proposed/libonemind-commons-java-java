/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.Map;
/**
 * A simple Map.Entry implementation that can be used for Map extension
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MapEntry.java,v 1.3 2005/04/26 17:41:48 thlee Exp $ $Name:  $
 */
public class MapEntry implements Map.Entry
{

    /** the key * */
    private Object _key;

    /** the value * */
    private Object _value;

    /**
     * {@inheritDoc}
     */
    public MapEntry(Object key, Object value)
    {
        _key = key;
        _value = value;
    }

    /**
     * {@inheritDoc}
     */
    public Object getKey()
    {
        return _key;
    }

    /**
     * {@inheritDoc}
     */
    public Object getValue()
    {
        return _value;
    }

    /**
     * {@inheritDoc}
     */
    public Object setValue(Object value)
    {
        Object o = _value;
        _value = value;
        return o;
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode()
    {
        return _key.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o)
    {
        return _key.equals(((MapEntry) o)._key);
    }
}