/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.*;
/**
 * Most recently used map (implementation based on mrulist)
 * 
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MruMap.java,v 1.3 2004/10/31 15:57:55 thlee Exp $ $Name:  $
 */
public class MruMap extends HashMap implements Map
{

    /**
     * For MruMap implementation
     * @author TiongHiang Lee (thlee@onemindsoft.org)
     * @version $Id: MruMap.java,v 1.3 2004/10/31 15:57:55 thlee Exp $ $Name:  $
     */
    private class InnerMruList extends MruList
    {
        /**
         * Constructor
         * @param size the size
         * @param timeout the timeout
         */
        public InnerMruList(long size, long timeout)
        {
            super(size, timeout);
        }
        
        /**
         * {@inheritDoc}
         */
        protected void truncateEntry(Object obj)
        {
            super.truncateEntry(obj);
            MruMap.this.remove(obj);
        }

        /**
         * {@inheritDoc}
         */
        protected void expireEntry(Object obj)
        {
            super.expireEntry(obj);
            MruMap.this.remove(obj);
        }
    }

    /** the list * */
    private MruList _mruList;

    /**
     * {@inheritDoc}
     */
    public MruMap()
    {
        this(0, 0);
    }

    /**
     * Constructor
     * @param size the limit of the map (0 for never timeout)
     * @param timeout the timeout (0 for never expire)
     */
    public MruMap(long size, long timeout)
    {
        _mruList = new InnerMruList(size, timeout);
    }

    /**
     * {@inheritDoc}
     */
    public void clear()
    {
        super.clear();
        _mruList.clear();
    }

    /**
     * {@inheritDoc}
     */
    public Object get(Object key)
    {
        _mruList.access(key);
        return super.get(key);
    }

    /**
     * {@inheritDoc}
     */
    public Object put(Object key, Object value)
    {
        Object result = super.put(key, value);
        _mruList.access(key); //this must be done second
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public void putAll(Map t)
    {
        super.putAll(t);//this must be done second
        _mruList.addAll(t.keySet());
        
    }

    /**
     * {@inheritDoc}
     */
    public Object remove(Object key)
    {
        _mruList.remove(key); 
        return super.remove(key);
    }

    /**
     * {@inheritDoc}
     */
    public void expire(MruList list, Object obj)
    {
        super.remove(obj);
    }

    /**
     * {@inheritDoc}
     */
    public void truncate(MruList list, Object obj)
    {
        super.remove(obj);
    }
}