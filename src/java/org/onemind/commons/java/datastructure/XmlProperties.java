
package org.onemind.commons.java.datastructure;

import java.io.*;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.*;
import org.onemind.commons.java.lang.reflect.ReflectUtils;
import org.onemind.commons.java.xml.digest.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * An XmlPropertiesReader read a properties out of an xml file. The xml is read
 * using dom parser. It support all the java primitive types 
 * and in addition creation of instance of ElementDigester
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: XmlProperties.java,v 1.3 2005/06/22 22:57:52 thlee Exp $ $Name:  $
 */
public class XmlProperties extends HashMap
{
    /**
     * Constructor
     * @param filename the file name
     * @throws FileNotFoundException
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public XmlProperties(String filename) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException
    {
        this(new FileInputStream(filename));
    }

    /**
     * Constructor
     * @param stream
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public XmlProperties(InputStream is) throws ParserConfigurationException, SAXException, IOException
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        // Parse the input
        SAXParser saxParser = factory.newSAXParser();
        SaxDigesterHandler handler = new SaxDigesterHandler();
        handler.addDigester("Properties", new XmlPropertyElementDigester("Property", this));
        saxParser.parse(is, handler);
    }
}