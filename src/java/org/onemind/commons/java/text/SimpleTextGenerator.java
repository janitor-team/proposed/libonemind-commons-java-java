/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.text;

import java.util.HashMap;
import java.util.Map;
/**
 * A simple implementation of generator
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class SimpleTextGenerator implements TextGenerator
{

    /** the delimiter **/
    private String _delimiter;

    /** sub delimiter **/
    private String _subDelimiter;

    /** attr generators * */
    private Map _generators = new HashMap();

    /**
     * Constructor
     */
    public SimpleTextGenerator(String delimiter, String subDelimiter)
    {
        _delimiter = delimiter;
        _subDelimiter = subDelimiter;
    }

    /**
     * add text generator to the sub specification
     * @param subSpec the sub spec
     * @param gen the sub generator
     */
    public void addGenerator(String subSpec, TextGenerator gen)
    {
        _generators.put(subSpec, gen);
    }

    /**
     * {@inheritDoc}
     */
    public StringBuffer generateText(String spec, Object obj)
    {
        StringBuffer sb = new StringBuffer();
        generateText(spec, obj, sb);
        return sb;
    }

    /**
     * {@inheritDoc}
     */
    public void generateText(String spec, Object obj, StringBuffer sb)
    {
        String[] fields = spec.split(_delimiter);
        for (int i = 0; i < fields.length; i++)
        {
            if (_subDelimiter != null)
            {
                TextGenerator gen = getGenerator(fields[i]);
                if (gen == null)
                {
                    throw new IllegalArgumentException("No sub generator for " + fields[i]);
                } else
                {
                    gen.generateText(null, obj, sb);
                }
            } else
            {
                String specs[] = fields[i].split(_subDelimiter);
                TextGenerator subGen = getGenerator(specs[0]);
                if (subGen == null)
                {
                    throw new IllegalArgumentException("No sub generator for " + specs[0]);
                }
                if (specs.length > 1)
                {
                    subGen.generateText(specs[1], obj, sb);
                } else
                {
                    subGen.generateText(null, obj, sb);
                }
            }
        }
    }

    /** 
     * Get the generator for subSpec
     * @param subSpec the sub spec
     * @return the generator for the sub spec
     */
    public TextGenerator getGenerator(String subSpec)
    {
        return (TextGenerator) _generators.get(subSpec);
    }
}