/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.util;

import java.io.IOException;
import java.io.Writer;
import java.util.*;
import java.util.HashMap;
import java.util.Map;
import org.onemind.commons.java.lang.MutableLong;
/**
 * For counting things
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: Counter.java,v 1.2 2005/06/22 22:58:25 thlee Exp $ $Name:  $
 */
public class Counter
{

    /** the counts **/
    private final Map counts = new HashMap();

    /**
     * Constructor
     */
    public Counter()
    {
    }

    /**
     * Add count. Count as 1 if it is not counted before
     * @param counted
     */
    public void count(Object counted)
    {
        count(counted, 1);
    }

    /**
     * Adjust count by value
     * @param counted the counted
     * @param countValue the count value
     */
    public void count(Object counted, long countValue)
    {
        MutableLong count = (MutableLong) counts.get(counted);
        if (count == null)
        {
            counts.put(counted, new MutableLong(countValue));
        } else
        {
            count.inc(countValue);
        }
    }

    /**
     * Remove the count. Count as -1 if it is not counted before
     * @param counted the counted
     */
    public void removeCount(Object counted)
    {
        MutableLong count = (MutableLong) counts.get(counted);
        if (count == null)
        {
            counts.put(counted, new MutableLong(-1));
        } else
        {
            count.dec(1);
        }
    }

    /**
     * Get the count
     * @param counted the counted
     * @return the count
     */
    public long getCount(Object counted)
    {
        MutableLong count = (MutableLong) counts.get(counted);
        if (count == null)
        {
            return 0;
        } else
        {
            return count.longValue();
        }
    }

    /**
     * Dump to output
     * @param writer the writer
     */
    public void dump(Writer writer) throws IOException
    {
        MapUtils.dump(counts, writer);
    }

    /**
     * Reset the count for counted
     * @param counted the counted
     */
    public void resetCount(Object counted)
    {
        counts.remove(counted);
    }

    /**
     * Reset all the counters
     */
    public void resetAll()
    {
        counts.clear();
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer(super.toString());
        sb.append(" - [");
        Iterator it = counts.keySet().iterator();
        while (it.hasNext())
        {
            Object obj = it.next();
            sb.append(obj);
            sb.append("=");
            sb.append(counts.get(obj));
            if (it.hasNext())
            {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}