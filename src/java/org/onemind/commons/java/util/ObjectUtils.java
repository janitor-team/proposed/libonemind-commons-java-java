/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */
package org.onemind.commons.java.util;

/**
 * Property utilities
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ObjectUtils.java,v 1.2 2004/08/26 12:33:17 thlee Exp $ $Name:  $
 */
public final class ObjectUtils
{

    /**
     * Constructor
     */
    private ObjectUtils()
    {
        super();
    }

    /**
     * Convert the object the boolean, return the default if it cannot be converted
     * @param value the value
     * @param def the default
     * @return the converted value, or def if value is null
     */
    public static final boolean toBool(Object value, boolean def)
    {
        if (value instanceof Boolean)
        {
            return ((Boolean) value).booleanValue();
        } else if (value instanceof String)
        {
            if ("TRUE".equalsIgnoreCase((String) value))
            {
                return true;
            } else if ("FALSE".equalsIgnoreCase((String) value))
            {
                return false;
            } else
            {
                return def;
            }
        } else
        {
            return def;
        }
    }

    /**
     * Convert the object to int, or return the default if it cannot be converted
     * @param value the value
     * @param def the default
     * @return the converted int, or default if it cannot be converted
     */
    public static int toInt(Object value, int def)
    {
        if (value instanceof Integer)
        {
            return ((Integer) value).intValue();
        } else if (value instanceof String)
        {
            try
            {
                return Integer.parseInt((String) value);
            } catch (NumberFormatException e)
            {
                return def;
            }
        } else
        {
            return def;
        }
    }

    /**
     * Convert the object to long, or return the default if it cannot be converted
     * @param value the value
     * @param def the default
     * @return the converted int, or default if it cannot be converted
     */
    public static long toLong(Object value, long def)
    {
        if (value instanceof Long)
        {
            return ((Long) value).longValue();
        } else if (value instanceof String)
        {
            try
            {
                return Long.parseLong((String) value);
            } catch (NumberFormatException e)
            {
                return def;
            }
        } else
        {
            return def;
        }
    }

    /**
     * Convert the object to float, or return the default if it cannot be converted
     * @param value the value
     * @param def the default
     * @return the converted int, or default if it cannot be converted
     */
    public static float toFloat(Object value, float def)
    {
        if (value instanceof Float)
        {
            return ((Float) value).floatValue();
        } else if (value instanceof String)
        {
            try
            {
                return Float.parseFloat((String) value);
            } catch (NumberFormatException e)
            {
                return def;
            }
        } else
        {
            return def;
        }
    }

    /**
     * Convert the object to double, or return the default if it cannot be converted
     * @param value the value
     * @param def the default
     * @return the converted int, or default if it cannot be converted
     */
    public static double toDouble(Object value, double def)
    {
        if (value instanceof Double)
        {
            return ((Double) value).doubleValue();
        } else if (value instanceof String)
        {
            try
            {
                return Double.parseDouble((String) value);
            } catch (NumberFormatException e)
            {
                return def;
            }
        } else
        {
            return def;
        }
    }
}