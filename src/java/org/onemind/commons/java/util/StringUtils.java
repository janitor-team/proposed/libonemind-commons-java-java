/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.util;

import java.util.Collection;
/**
 * String utilities method
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: StringUtils.java,v 1.3 2004/09/19 20:07:32 thlee Exp $ $Name:  $
 */
public final class StringUtils
{

    /**
     * Constructor
     */
    private StringUtils()
    {
        super();
    }

    /**
     * Concat the collection l to string with delimenter
     * @param l the collection
     * @param delimiter the delimiter
     * @return the result string
     */
    public static String concat(Collection l, String delimiter)
    {
        return concat(l.toArray(), delimiter);
    }

    /**
     * Concant the object in the array (using objects.toString()), delimited by delimiter
     * @param objects the objects
     * @param delimiter the delimiter
     * @return a string
     */
    public static String concat(Object[] objects, String delimiter)
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < objects.length; i++)
        {
            sb.append(objects[i]);
            if (i < (objects.length - 1))
            {
                sb.append(delimiter);
            }
        }
        return sb.toString();
    }

    /**
     * Return the substring after the first occurrance of pattern
     * @param str the str
     * @param pattern the pattern
     * @return the substring of pattern can be matched in str, or null
     */
    public static String substringAfter(String str, String pattern)
    {
        int i = str.indexOf(pattern);
        if (i == -1)
        {
            return null;
        }
        return str.substring(i + pattern.length());
    }

    /**
     * Return substring of str after the the last occurrance of pattern
     * @param str the str
     * @param pattern the pattern
     * @return the substring if pattern can be matched in the str, or null
     */
    public static String substringAfterLast(String str, String pattern)
    {
        int i = str.lastIndexOf(pattern);
        if (i == -1)
        {
            return null;
        }
        return str.substring(i + pattern.length());
    }

    /**
     * Return substring of str before the last occurrance of str
     * @param str the str
     * @param pattern the pattern
     * @return the substring if pattern can be matched, or null
     */
    public static String substringBeforeLast(String str, String pattern)
    {
        int i = str.lastIndexOf(pattern);
        if (i == -1)
        {
            return null;
        }
        return str.substring(0, i);
    }

    /**
     * Whether the string is empty
     * @param str whether is null or zero length (after trim)
     * @return true if null of zero length after trim
     */
    public static boolean isNullOrEmpty(String str)
    {
        return (str == null || str.trim().length() == 0);
    }

    /**
     * Whether the string is empty
     * @param str whether is null or zero length (after trim)
     * @return true if null of zero length after trim
     */
    public static boolean isNullOrEmpty(Object strObject)
    {
        if (strObject == null)
        {
            return true;
        } else if (strObject instanceof String)
        {
            return isNullOrEmpty((String) strObject);
        } else
        {
            return isNullOrEmpty(strObject.toString());
        }
    }
}