/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.xml.digest;

import java.util.EventObject;
import org.onemind.commons.java.event.*;
import org.onemind.commons.java.event.EventFirer;
import org.onemind.commons.java.event.EventListenerList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * The abstract implementation of ElementCreatorDigester. The element creator will fire an event
 * at the end of digestion
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public abstract class AbstractElementCreatorDigester extends DefaultDigester implements ElementCreatorDigester
{
    /** the listener list **/
    private final EventListenerList _listeners = new EventListenerList();

    /** the created object **/
    private Object _created;

    /** event firer for element event **/
    private static final EventFirer _FIRER = new EventFirer()
    {

        /**
         * {@inheritDoc}
         */
        public void fireEvent(EventListener listener, EventObject evt)
        {
            ((ElementListener) listener).objectCreated(((ElementEvent) evt).getElement());
        }
    };

    /**
     * Constructor
     * @param name the element name
     */
    public AbstractElementCreatorDigester(String name)
    {
        super(name);
    }

    /**
     * {@inheritDoc}
     */
    public final void addListener(ElementListener l)
    {
        _listeners.addListener(l);
    }

    /**
     * {@inheritDoc}
     */
    public final void removeListener(ElementListener l)
    {
        _listeners.removeListener(l);
    }

    /** 
     * {@inheritDoc}
     */
    public void endDigest(SaxDigesterHandler handler) throws SAXException
    {
        _listeners.fireEvent(_FIRER, new ElementEvent(this, getCreatedElement()));
    }

    /**
     * Set the created element
     * @param obj the object
     */
    protected final void setCreatedElement(Object obj)
    {
        _created = obj;
    }

    /** 
     * {@inheritDoc}
     */
    public final Object getCreatedElement()
    {
        return _created;
    }
}