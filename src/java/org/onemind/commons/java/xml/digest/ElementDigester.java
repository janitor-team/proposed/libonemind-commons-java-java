/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@thinklient.org
 */

package org.onemind.commons.java.xml.digest;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * An element digester digest a particular element inside an xml file in common-digest framework
 * @author TiongHiang Lee (thlee@thinklient.org)
 * @version $Id: ElementDigester.java,v 1.3 2005/06/22 22:58:58 thlee Exp $ $Name:  $
 */
public interface ElementDigester
{
    /**
     * Get the element name
     * @return the element name
     */
    public String getElementName();
    
    /**
     * Start an element with the given attributes
     * @param handler the handler
     * @param attr the attributes
     * @throws SAXException if there's handling exception
     */
    public void startDigest(SaxDigesterHandler handler, Attributes attr) throws SAXException;

    /**
     * End the element
     * @param handler the handler
     * @throws SAXException if there's handling exception
     */
    public void endDigest(SaxDigesterHandler handler) throws SAXException;

    /**
     * Handle the characters
     * @param handler the handler
     * @param chars the characters
     * @param offset the offset
     * @param length the length
     * @throws SAXException if there's parse problem
     */
    public void characters(SaxDigesterHandler handler, char[] chars, int offset, int length) throws SAXException;
}