/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import junit.framework.TestCase;
/**
 * Unit test for BiMap
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: BiMapTest.java,v 1.1 2004/09/29 02:45:36 thlee Exp $ $Name:  $
 */
public class BiMapTest extends TestCase
{

    /**
     * Test bimap functionality
     * @throws Exception
     */
    public void testBiMap() throws Exception
    {
        BiMap biMap = new BiMap();
        biMap.put("1", "one");
        //assert values is correct
        BiMap inverse = biMap.getInverse();
        assertEquals("one", biMap.get("1"));
        assertEquals("1", inverse.get("one"));
        try
        {
            //test conflicts
            biMap.put("2", "one");
            throw new Exception("BiMap should not accept non-unique value");
        } catch (IllegalArgumentException e)
        {
            //expected
        }
        //test insert different value
        biMap.put("1", "two");
        assertEquals("two", biMap.get("1"));
        assertEquals("1", inverse.get("two"));
        //test remove
        biMap.remove("1");
        assertEquals(biMap.get("1"), null);
        assertEquals(inverse.get("one"), null);
    }
}