/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.ArrayList;
import java.util.List;
import org.onemind.commons.java.datastructure.ClassSet;
import junit.framework.TestCase;
/**
 * Testing for ClassSet
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ClassSetTest.java,v 1.2 2004/08/26 12:33:09 thlee Exp $ $Name:  $
 */
public class ClassSetTest extends TestCase
{

    public void testIsSubClasses()
    {
        ClassSet set = new ClassSet();
        set.add(Number.class);
        assertTrue(!set.isSubclassOfClasses(Object.class));
        assertTrue(set.isSubclassOfClasses(Number.class));
    }

    public void testAddAll()
    {
        List l = new ArrayList();
        l.add(Object.class);
        l.add(Number.class);
        ClassSet set = new ClassSet();
        set.addAll(l);
        l.add(new Object());
        try
        {
            set.addAll(l);
            throw new Exception("Object should have be accepted by addAll()");
        } catch (Exception e)
        {
            //pass
        }
    }
}