/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.List;
import junit.framework.TestCase;
import org.onemind.commons.java.datastructure.CounterQueue;
/**
 * The test for counter queue
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: CounterQueueTest.java,v 1.2 2004/08/26 12:33:09 thlee Exp $ $Name:  $
 */
public class CounterQueueTest extends TestCase
{

    public void testCounterQueue()
    {
        CounterQueue cq = new CounterQueue();
        Object counter = new Object();
        Object queuer1 = new Object();
        Object queuer2 = new Object();
        //just check for random counter object
        assertEquals(cq.getQueue(new Object()).size(), 0);
        //adding
        cq.addToQueue(counter, queuer1);
        cq.addToQueue(counter, queuer2);
        //just check for random counter object
        assertEquals(cq.getQueue(new Object()).size(), 0);
        //test added result
        List l = cq.getQueue(counter);
        assertEquals(l.size(), 2);
        assertEquals(l.get(0), queuer1);
        assertEquals(l.get(1), queuer2);
        //test remove next from queue
        assertEquals(cq.removeNextFromQueue(counter), queuer1);
        assertEquals(l.size(), 1);
        //test remove from queuer
        cq.removeFromQueue(counter, queuer2);
        assertEquals(l.size(), 0);
    }
}