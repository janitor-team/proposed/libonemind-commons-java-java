/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.HashMap;
import java.util.Map;
import junit.framework.TestCase;
public class DuoMapKeyTest extends TestCase
{

    public void testKey()
    {
        Map m = new HashMap();
        Object o1 = new Object();
        Object o2 = new Object();
        m.put(new DuoMapKey(null, null), o1);
        assertEquals(m.get(new DuoMapKey(null, null)), o1);
        m.put(new DuoMapKey("test1", "test2"), o2);
        assertEquals(m.get(new DuoMapKey("test1", "test2")), o2);
        assertEquals(m.get(new DuoMapKey("test2", "test2")), null);
    }
}
