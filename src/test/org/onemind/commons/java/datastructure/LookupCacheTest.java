/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import org.onemind.commons.java.datastructure.LookupCache;
import junit.framework.TestCase;
/**
 * Test for lookup cache
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: LookupCacheTest.java,v 1.2 2004/08/26 12:33:09 thlee Exp $ $Name:  $
 */
public class LookupCacheTest extends TestCase
{

    /**
     * A test implemement for the lookup cache
     * @author TiongHiang Lee (thlee@onemindsoft.org)
     * @version $Id: LookupCacheTest.java,v 1.2 2004/08/26 12:33:09 thlee Exp $ $Name:  $
     */
    private class TestCacheImpl extends LookupCache
    {

        /**
         * Only produce value if key is "One" {@inheritDoc}
         */
        protected Object produce(Object key)
        {
            if (key.equals("One"))
            {
                return new Integer("1");
            } else
            {
                return null;
            }
        }
    }

    public void testLookupCache()
    {
        TestCacheImpl cache = new TestCacheImpl();
        String one = "One";
        String two = "Two";
        assertFalse(cache.isInCache(one));
        assertFalse(cache.isInCache(two));
        assertFalse(cache.isInNegCache(one));
        assertFalse(cache.isInNegCache(two));
        cache.lookup("One");
        cache.lookup("Two");
        assertTrue(cache.isInCache(one));
        assertTrue(cache.isInNegCache(two));
    }
}