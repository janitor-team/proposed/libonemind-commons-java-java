/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.TreeSet;
import org.onemind.commons.java.datastructure.MruList;
import junit.framework.TestCase;
/**
 * Test for MruList
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MruListTest.java,v 1.2 2004/08/26 12:33:09 thlee Exp $ $Name:  $
 */
public class MruListTest extends TestCase
{

    /**
     * Test the MruEntry implementation
     * @throws Exception if there's problem
     */
    public void testMruEntry() throws Exception
    {
        TreeSet set = new TreeSet();
        Object first = new String("first");
        Object second = new String("second");
        MruList.MruEntry entry1 = new MruList.MruEntry(first, System.currentTimeMillis());
        Thread.currentThread().sleep(20);
        MruList.MruEntry entry2 = new MruList.MruEntry(second, System.currentTimeMillis());
        set.add(entry1);
        set.add(entry2);
        assertEquals(set.iterator().next(), entry2);
        System.out.println(set);
        Thread.currentThread().sleep(20);
        entry1.setLastAccessTime(System.currentTimeMillis());
        set.remove(entry1);
        set.add(entry1);
        System.out.println(set);
        assertEquals(set.iterator().next(), entry1);
    }

    /**
     * Test the MruList
     * @throws Exception if there's problem
     */
    public void testMruList() throws Exception
    {
        MruList list = new MruList();
        Object first = new String("first");
        Object second = new String("second");
        list.add(first);
        Thread.currentThread().sleep(20);
        list.add(second);
        System.out.println(list.iterator().next());
        assertEquals(list.iterator().next(), second);
        Thread.currentThread().sleep(20);
        list.add(first);
        assertEquals(list.iterator().next(), first);
    }

    /**
     * Test truncation
     */
    public void testTruncate()
    {
        MruList list  = new MruList(3, 0);
        for (int i = 0; i < 10; i++)
        {
            Integer obj = new Integer(i);
            list.add(obj);
        }
        assertEquals(list.size(), 3);
    }
}