/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import org.onemind.commons.java.datastructure.MruMap;
import junit.framework.TestCase;
/**
 * Test for MruMap
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MruMapTest.java,v 1.2 2004/08/26 12:33:08 thlee Exp $ $Name:  $
 */
public class MruMapTest extends TestCase
{
    public void testMruMap() throws Exception
    {
        MruMap map = new MruMap(3, 0);
        for (int i = 0; i < 10; i++)
        {
            Integer obj = new Integer(i);
            Thread.sleep(200);
            map.put(obj, obj);
        }
        assertEquals(map.size(), 3);
        assertTrue(map.containsKey(new Integer(9)));
        assertTrue(map.containsKey(new Integer(8)));
        assertTrue(map.containsKey(new Integer(7)));
    }
}