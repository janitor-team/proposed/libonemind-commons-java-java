/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.HashMap;
import java.util.Map;
import org.onemind.commons.java.datastructure.TrackedMap;
import junit.framework.TestCase;
/**
 * Test for TrackedMap
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: TrackedMapTest.java,v 1.2 2004/08/26 12:33:09 thlee Exp $ $Name:  $
 */
public class TrackedMapTest extends TestCase
{

    public void testTrackedMap()
    {
        Object first = new String("first");
        Object second = new String("second");
        Object third = new String("third");
        Map m = new HashMap();
        m.put(first, first);
        
        //start with first
        TrackedMap map = new TrackedMap(m);
        assertEquals(map.size(), 1);
        assertEquals(map.keySet().size(), 1);
        assertEquals(map.values().size(), 1);
        //replace first
        map.put(first, first);
        assertTrue(map.hasChanges());
        assertEquals(map.size(), 1);
        assertEquals(map.keySet().size(), 1);
        assertEquals(map.values().size(), 1);
        
        //add second
        map.put(second, second);
        //assert that it's changed
        assertTrue(map.hasChanges());
        assertEquals(map.size(), 2);
        assertEquals(map.keySet().size(), 2);
        assertEquals(map.values().size(), 2);
        
        //make up-to-date
        map.makeUpToDate();
        assertTrue(!map.hasChanges());     
        assertEquals(map.size(), 2);
        assertEquals(map.keySet().size(), 2);
        assertEquals(map.values().size(), 2);
    }
}