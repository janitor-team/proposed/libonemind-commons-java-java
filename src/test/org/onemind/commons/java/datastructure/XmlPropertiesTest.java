/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import org.onemind.commons.java.xml.digest.DefaultDigester;
import org.onemind.commons.java.xml.digest.SaxDigesterHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import junit.framework.TestCase;
/**
 * Xml properties
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: XmlPropertiesTest.java,v 1.3 2005/06/22 22:59:13 thlee Exp $ $Name:  $
 */
public class XmlPropertiesTest extends TestCase
{

    public static class TestElement extends DefaultDigester
    {

        public boolean processed = false;

        /**
         * Constructor
         */
        public TestElement()
        {
            super("TestElement");
        }

        /** 
         * {@inheritDoc}
         */
        public void startDigest(SaxDigesterHandler handler, Attributes attrs) throws SAXException
        {
            processed = true;
        }
    }

    public void testXmlProperties() throws Exception
    {
        XmlProperties prop = new XmlProperties(getClass().getResourceAsStream("XmlProperties.xml"));
        assertEquals("default", prop.get("defaultvalue"));
        assertEquals(new Short((short) 1), prop.get("shortvalue"));
        assertEquals(new Integer(1), prop.get("intvalue"));
        assertEquals(new Long(1), prop.get("longvalue"));
        assertEquals(new Float(1), prop.get("floatvalue"));
        assertEquals(new Double(1), prop.get("doublevalue"));
        assertEquals(new Character('c'), prop.get("charvalue"));
        assertEquals("string", prop.get("stringvalue"));
        TestElement e = (TestElement) prop.get("element");
        assertEquals(e.processed, true);
    }
}