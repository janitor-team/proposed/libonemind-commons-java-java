/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */
package org.onemind.commons.java.lang.reflect;

import org.onemind.commons.java.lang.reflect.ClassLookupCache;
import junit.framework.TestCase;


/**
 * Unit test the class lookup cache
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ClassLooupCacheTest.java,v 1.4 2006/10/29 17:02:38 thlee Exp $ $Name:  $
 */
public class ClassLooupCacheTest extends TestCase
{
    /** the cache **/
    private ClassLookupCache _cache;

    /**
     * {@inheritDoc}
     */
    public void setUp()
    {
        _cache = new ClassLookupCache();
        _cache.addImport("java.io.*");
    }

    /**
     * Test lookup
     */
    public void testClassLookup() throws Exception
    {
        Class c = _cache.getClass("File");
        assertEquals(c, java.io.File.class);
        c = _cache.getClass("File1");
        assertEquals(c, null);

        //do it again to test the cache behavior.
        //result verified through log4j output
        assertTrue(_cache.isInCache("java.io.File"));        
        c = _cache.getClass("File1");
        assertEquals(c, null);
        
        c = _cache.getClass("Dummy");
        assertEquals(c, null);
        
        _cache.addImport("*");
        c = _cache.getClass("Dummy");
        assertEquals(Class.forName("Dummy"), c);
    }
}
