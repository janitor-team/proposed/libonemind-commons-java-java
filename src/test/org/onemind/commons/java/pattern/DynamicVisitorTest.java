
package org.onemind.commons.java.pattern;

import junit.framework.TestCase;
/**
 * @author TiongHiang Lee (tlee@i2rd.com)
 * @version $Id: DynamicVisitorTest.java,v 1.1 2004/10/31 16:03:44 thlee Exp $ $Name:  $
 */
public class DynamicVisitorTest extends TestCase
{

    private class TestBaseClass
    {
    }

    public class TestClass extends TestBaseClass
    {
    }

    public class TestSubClass extends TestClass
    {
    }

    public class TestVisitor extends DynamicVisitor
    {

        /** 
         * {@inheritDoc}
         */
        protected void initNodeHandlers()
        {
            addMethodNodeHandler(TestBaseClass.class, "visit");
            addMethodNodeHandler(TestClass.class, "visit");
            addMethodNodeHandler(TestSubClass.class, "visit");
        }

        public Object visit(TestBaseClass obj, Object[] data)
        {
            assertTrue(obj.getClass()==TestBaseClass.class);
            return null;
        }

        public Object visit(TestClass obj, Object[] data)
        {
            assertTrue(obj.getClass()==TestClass.class);
            return null;
        }

        public Object visit(TestSubClass obj, Object[] data)
        {
            assertTrue(obj.getClass()==TestSubClass.class);
            return null;
        }
    };

    public void testVisitor()
    {
        TestVisitor visitor = new TestVisitor();
    }
}