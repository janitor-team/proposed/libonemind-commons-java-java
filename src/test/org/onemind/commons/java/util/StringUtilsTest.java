/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.util;

import java.util.ArrayList;
import java.util.List;
import org.onemind.commons.java.util.StringUtils;
import junit.framework.TestCase;
/**
 * Test for string utilities
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: StringUtilsTest.java,v 1.2 2004/08/26 12:33:17 thlee Exp $ $Name:  $
 */
public class StringUtilsTest extends TestCase
{

    public void testConcatCollection()
    {
        List l = new ArrayList();
        l.add(new String("first"));
        l.add(new String("second"));
        l.add(new String("third"));
        String result = StringUtils.concat(l, ",");
        assertEquals(result, "first,second,third");
    }

    public void testSubStringAfterLast()
    {
        String result = StringUtils.substringAfter("abcdefg", "cd");
        assertEquals(result, "efg");
    }

    public void testSubStringBeforeLast()
    {
        String result = StringUtils.substringBeforeLast("abcdefg", "cd");
        assertEquals(result, "ab");
    }
}